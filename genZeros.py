#!/usr/bin/python3

import sys
from mpmath import *

def genZetaZeros(x, d):
    mp.prec = 2*(d+1)+100
    mp.dec = 2*(d+1)+100
    zero = zetazero(x).imag
    idx = str(zero).index(".")
    zero = str(zero)[idx-1:]
    zero = zero.replace(".","")
    zz = zero[d]
    return int(zz)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gmp.h>
#include "pi.h"
#include "e.h"
#include "zetazeros.h"
#define PY_SSIZE_T_CLEAN
#include <Python.h>
#define EXPANSION_FACTOR 3//10
#define FLOAT_LENGTH 7

char* strrev(char* str) {
	int l = strlen(str);
	int i = 0;
	char* _str = strdup(str);
	while (i < (l+1)/2) {
		char t = _str[i];
		_str[i] = _str[l - 1 - i];
		_str[l - 1 - i] = t;
		++i;
	}
	return _str;
}

void characterize(char* num, char* half, FILE* f_pi, FILE* f_e, FILE* pivots) {
	char pk = 0;
	char ek = 0;
	int pcnt = 0;
	int ctr = 0;
	int ll = strlen(num);
	int l = strlen(half);
	int prevVal = -1;
	while (((fscanf(f_pi, "%c", &pk)!=EOF) && (fscanf(f_e, "%c", &ek)!=EOF))) {
		if (pk == '.') {
			fscanf(f_pi, "%c", &pk);
			fscanf(f_e, "%c", &ek);
		}
		char nk = num[pcnt % ll];
		char cn[3];
		cn[0] = pk;
		cn[1] = nk;
		cn[2] = '\0';
		int index = atoi(cn);
		if (index == 0) index = 100;
		long long int pVal = 0;
		PyObject *pName, *pModule, *pFunc;
		PyObject *pArgs, *pValue;
		Py_Initialize();
		PyRun_SimpleString("import sys");
		PyRun_SimpleString("sys.path.append(\".\")");
		PyRun_SimpleString("sys.path.append(\"/usr/local/lib/python3.8/dist-packages\")");
		pName = PyUnicode_DecodeFSDefault("genZeros");
		pModule = PyImport_Import(pName);
		Py_DECREF(pName);
		if (pModule != NULL) {
			pFunc = PyObject_GetAttrString(pModule, "genZetaZeros");
			pArgs = PyTuple_New(2);
			pValue = PyLong_FromLong(index);
			PyTuple_SetItem(pArgs, 0, pValue);
			pValue = PyLong_FromLong(ek-'0');
			PyTuple_SetItem(pArgs, 1, pValue);
			pValue = PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
			pVal = PyLong_AsLong(pValue);
			Py_DECREF(pValue);
			Py_XDECREF(pFunc);
			Py_DECREF(pModule);
		}
		Py_FinalizeEx();
		int expected_nk = half[ctr]-'0';
		printf("\n%d\t%c\t%d\t%lld\t", index, ek, prevVal, pVal);
		//system("a=1;read a");
		if (prevVal > -1 && pVal == prevVal && prevVal!=expected_nk) {
			printf("\n Previous hit\n");
			fprintf(pivots, "%d\n", pcnt);
			prevVal = expected_nk;
		} else if (prevVal > -1 && expected_nk == pVal) {
			printf("\n Current hit\n");
			fprintf(pivots, "%d\n", pcnt);
			prevVal = pVal;
			ctr = ctr + 1;
			if (ctr >= l) {
				return;
			}
		} else if (prevVal == -1 && expected_nk == pVal) {
			printf("\n Current hit\n");
			prevVal = pVal;
			fprintf(pivots, "%d\n", pcnt);
			ctr = ctr + 1;
			if (ctr >= l) {
				return;
			}
		}
                pcnt = pcnt + 1;		
	}
	return;
}

void factorize(FILE* p1, FILE* p2, FILE* f1, FILE* f2) {
	int pivot = 0;
	int prev = -1;
	int t = 0;
	while ((fscanf(p1, "%d", &pivot)!= EOF)) {
		if (prev > -1) {
			if (pivot-prev > 1) {
				if (t == 0) {
					fprintf(f1, "%d", pivot-prev-1);
				} else {
					fprintf(f2, "%d", pivot-prev-1);
				}
				prev = pivot;
			}
		} else {
			prev = pivot;
		}
		t = 1 - t;
	}
	prev = -1;
	while ((fscanf(p2, "%d", &pivot)!= EOF)) {
		if (prev > -1) {
			if (pivot-prev > 1) {
				if (t == 0) {
					fprintf(f1, "%d", pivot-prev-1);
				} else {
					fprintf(f2, "%d", pivot-prev-1);
				}
				prev = pivot;
			}
		} else {
			prev = pivot;
		}
		t = 1 - t;
	}
	return;
}

int main(int argc, char* argv[]) {
	wchar_t *program = Py_DecodeLocale(argv[0], NULL);
	if (program == NULL) {
		fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
		exit(1);
	}
	Py_SetProgramName(program);  /* optional but recommended */
	char* num = strdup(argv[1]);
	int l = strlen(num);
	int mid = (l + 1) / 2;
	char* first_half = (char*) calloc(mid + 1, sizeof(char));
	char* second_half = (char*) calloc(mid + 1, sizeof(char));
	char* rnum = strrev(num);
	strncpy(first_half, num, mid);
	strncpy(second_half, rnum, mid);
	first_half[mid] = '\0';
	second_half[mid] = '\0';
	printf("\nNumber to be factored : %s \t%s\n", first_half, second_half);
	FILE* f_pi = fopen("/home/jft-dev02/Projects/intfact/pi.txt","r");
	FILE* f_e = fopen("/home/jft-dev02/Projects/intfact/e.txt","r");
	FILE* pivots1 = fopen("/home/jft-dev02/Projects/intfact/pivots1.txt","w");
	characterize(num,first_half, f_pi, f_e, pivots1);
	fclose(f_e);
	fclose(f_pi);
	fclose(pivots1);
	f_pi = fopen("/home/jft-dev02/Projects/intfact/pi.txt","r");
	f_e = fopen("/home/jft-dev02/Projects/intfact/e.txt","r");
	FILE* pivots2 = fopen("/home/jft-dev02/Projects/intfact/pivots2.txt","w");
	characterize(num,second_half, f_pi, f_e, pivots2);
	fclose(f_e);
	fclose(f_pi);
	fclose(pivots2);
	pivots1 = fopen("/home/jft-dev02/Projects/intfact/pivots1.txt","r");
	pivots2 = fopen("/home/jft-dev02/Projects/intfact/pivots2.txt","r");
	FILE* factor2 = fopen("/home/jft-dev02/Projects/intfact/factor2.txt","w");
	FILE* factor1 = fopen("/home/jft-dev02/Projects/intfact/factor1.txt","w");
	factorize(pivots1, pivots2, factor1, factor2);
	fclose(factor1);
	fclose(factor2);
	fclose(pivots1);
	fclose(pivots2);
	PyMem_RawFree(program);
	return 0;
}
